import {
    WebGLRenderer,
    PerspectiveCamera,
    Scene,
    Mesh,
    PlaneBufferGeometry,
    ShadowMaterial,
    MeshStandardMaterial,
    DirectionalLight,
    PCFSoftShadowMap,
    sRGBEncoding,
    Color,
    AmbientLight,
    Box3,
    LoadingManager,
    MathUtils,
    SphereGeometry,
    MeshBasicMaterial,
    Vector3,
} from 'three';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls.js';
import { GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader.js';
import URDFLoader from '../../src/URDFLoader.js';
import * as Paho from '../vendor/mqttws31.js';
import { InverseKinematic } from '../../src/InverseKinematic.js';

// srb - style red background, srt - style red text
const srb = 'font-weight: bold; background-color: #831010; color: white;';
const srt = 'font-weight: bold; color: #831010;';
const sgb = 'font-weight: bold; background-color: #438134; color: white;';
const sgt = 'font-weight: bold; color: #438134;';
const sbb = 'font-weight: bold; background-color: black; color: white;';
// style italic
const si = 'text-decoration: italic;';

const robotJointInfo = {
    q0: [
        'Kabine_zu_Radlauf2',
        0,
        [-Math.PI, Math.PI],
    ],
    q1: [
        'BaggerVerbindungArm2_zu_BaggerVerbindungArm',
        0,
        [-Math.PI / 4, Math.PI / 4],
    ],
    q2: [
        'ZylAusleger_zu_ZylAuslegerKolbenstange',
        -0.09868047363131909,
        [-0.2, 0.0],
    ],
    q3: [
        'ZylArm_zu_ZylArmKolbenstange',
        -0.04181359230784053,
        [-0.41, 0.0],
    ],
    q4: [
        'ZylLoeffel_zu_ZylLoeffelKolbenstange',
        -0.12725655458018606,
        [-0.32, 0.0],
    ],
    q5: [
        'ZylSchildKolbenstange_zu_ZylSchild',
        0.14,
        [0, 0.14],
    ],
};

function makeId(length) {
    let result = '';
    const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    const charactersLength = characters.length;
    for (let i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}

const knownParams = {
    local_broker: false,
};

// Parse options in a code block
{
    const urlSplit = location.href.split('?');
    if (urlSplit.length === 2) {
        let getParams = urlSplit[1];
        getParams = getParams.split('+');

        for (let param of getParams) {
            param = param.split('=');
            if (knownParams[param[0]] !== undefined) {
                knownParams[param[0]] =
                    (param.length === 1 && true) ||
                    (param.length === 2 && param[1]);
            }
        }
    }
}

console.dir(knownParams);

const brokerConnectParams = [
    knownParams.local_broker ? 'localhost' : 'broker.emqx.io',
    (location.protocol === 'https:' ? Number(8084) : Number(8083)),
    knownParams.local_broker ? '' : '/mqtt',
    knownParams.local_broker ? 'bcmaclient' : `BCMA-MQTT-CLIENT-${ makeId(6) }`];
const client = new Paho.MQTT.Client(...brokerConnectParams);
client.onConnectionLost = onConnectionLost;

const tokenLength = 14;
const token = makeId(tokenLength);
const inputTopic = `rwth/bcma/sessions/${ token }/command`;
const outputTopic = `rwth/bcma/sessions/${ token }/status`;

function onBrokerConnect() {
    // Once a connection has been made, make a subscription and send a message.
    const payload = `%cConnected to broker.emqx.io (token=${ token })`;
    console.log(payload, sbb);

    client.subscribe(inputTopic, {
        onSuccess: () => {
            console.log(`%cListening on topic "${ inputTopic }"`, sgb);
            client.onMessageArrived = onMessageArrived;
        },
        onFailure: () => { console.log(`%cUnable to listen on topic "${ inputTopic }"`, srb); },
    });
    console.log(`%cPublishing on topic "${ outputTopic }"`, sgb);

    document.getElementById('sessionKeyBCMA').innerHTML = token;
    document.getElementById('sessionMQTTBroker').classList.remove('disconnected');
    document.getElementById('sessionMQTTBroker').innerText =
        `${ brokerConnectParams[0] }:${ brokerConnectParams[1] }`;
}

const brokerConnectOptions = {
    useSSL: location.protocol === 'https:',
    onSuccess: onBrokerConnect,
};

// called when the client loses its connection
function onConnectionLost(responseObject) {
    if (responseObject.errorCode !== 0) {
        console.log(`onConnectionLost: ${ responseObject.errorMessage }`);
    }
    document.getElementById('sessionMQTTBroker').classList.add('disconnected');
    console.log('Reconnecting...');
    client.connect(brokerConnectOptions);
}

let scene, camera, renderer, robot, controls, ball, ball2;

// called when a message arrives
function onMessageArrived(message) {
    const payloadString = message.payloadString;
    console.debug(`%c[MQTT PAYLOAD]%c ${ payloadString }`, sbb, si);

    console.assert(robot.joints, {
        robot,
        reason: '"robot" undefined or does not a property "joints" in MQTT message handler.',
    });
    if (!robot.joints) return;

    let payload;

    try {
        payload = JSON.parse(message.payloadString);
        if (!payload.command) {
            // eslint-disable-next-line no-throw-literal
            throw 'JSON object received does not contain property "command"';
        }
    } catch (error) {
        console.log(`%c[MQTT]%c Malformed JSON received %c(${ error }).`, srb, srt, si);
        console.log('%c[RAW PAYLOAD]%c ' + message.payloadString, sbb, si);

        return;
    }
    // eslint-disable-next-line eqeqeq
    if (payload.command == 'setJointValue' ||
    // eslint-disable-next-line eqeqeq
        payload.command == 'getJointValue') {

        console.log(`%c[MQTT]%c command received %c'${ payload.command }'.`, sgb, sgt, sgt + si);

        console.assert(typeof payload.jointName === 'string' &&
            typeof robotJointInfo[payload.jointName] === 'object', {
            payload: payload,
            jointName: payload.jointName,
            reason: 'jointName isn\'t valid in payload.',
        });
        if (typeof payload.jointName !== 'string' ||
            typeof robotJointInfo[payload.jointName] !== 'object') {
            return;
        }

        const jointInfo = robotJointInfo[payload.jointName];
        const jointRealName = jointInfo[0];
        // eslint-disable-next-line eqeqeq
        if (payload.command == 'getJointValue') {

        // eslint-disable-next-line eqeqeq
        } else if (payload.command == 'setJointValue') {
            console.assert(typeof payload.jointValue === 'number', {
                payload: payload,
                jointValue: payload.jointValue,
                reason: 'jointValue isn\'t valid in pyload.',
            });
            if (typeof payload.jointValue !== 'number') {
                return;
            }
            const [jointMin, jointMax] = jointInfo[2];
            const jointValue = Math.max(Math.min(payload.jointValue, jointMax), jointMin);
            console.assert(robot.joints[jointRealName] &&
                robot.joints[jointRealName].setJointValue);
            robot.joints[jointRealName].setJointValue(jointValue);
            console.log(`%c[ROBOT]%c ${ jointRealName }:=${ jointValue }.`, sgb, sgt + si);
        }

    // eslint-disable-next-line eqeqeq
    } else if (payload.command == 'setTargetPosition') {
        console.log(`%c[MQTT]%c command received %c'${ payload.command }'.`, sgb, sgt, sgt + si);

        if (typeof payload.x === 'number') {
            ball.position.x = payload.x;
            console.log(`%ctarget x=${ payload.x }`, si);
        }
        if (typeof payload.y === 'number') {
            ball.position.y = payload.y;
            console.log(`%ctarget y=${ payload.y }`, si);
        }
        if (typeof payload.z === 'number') {
            ball.position.z = payload.z;
            console.log(`%ctarget z=${ payload.z }`, si);
        }
    // eslint-disable-next-line eqeqeq
    } else if (payload.command == 'setDummyPosition') {
        console.log(`%c[MQTT]%c command received %c'${ payload.command }'.`, sgb, sgt, sgt + si);

        if (typeof payload.x === 'number') {
            ball2.position.x = payload.x;
            console.log(`%cdummy x=${ payload.x }`, si);
        }
        if (typeof payload.y === 'number') {
            ball2.position.y = payload.y;
            console.log(`%cdummy y=${ payload.y }`, si);
        }
        if (typeof payload.z === 'number') {
            ball2.position.z = payload.z;
            console.log(`%cdummy z=${ payload.z }`, si);
        }
    // eslint-disable-next-line eqeqeq
    } else if (payload.command == 'calculateIK') {
        console.log(`%c[MQTT]%c command received %c'${ payload.command }'.`, sgb, sgt, sgt + si);
        console.dir(robot);
    } else {
        console.log(`%c[MQTT]%c Unknown command %c'${ payload.command }'`, srb, srt, srt + si);
    }
}

init();

let target = false;
function toggleTarget() {
    if (target) {
        target = false;
        scene.remove(ball);
    } else {
        target = true;
        scene.add(ball);
    }
}

let debug = false;
function toggleDebug() {
    if (debug) {
        debug = false;

        robot.traverse(c => {
            c.castShadow = true;
            c.traverse((o) => {
                if (o.type === 'Mesh') {
                    o.material.transparent = false;
                }
            });
        });

        scene.remove(ball2);
        document.getElementById('debug').style.display = 'none';
    } else {
        debug = true;

        robot.traverse(c => {
            c.castShadow = true;
            c.traverse((o) => {
                if (o.type === 'Mesh') {
                    o.material.transparent = true;
                    o.material.opacity = 0.5;
                }
            });
        });

        scene.add(ball2);
        document.getElementById('debug').style.display = 'block';
    }
}
function init() {

    // Load robot
    const manager = new LoadingManager();
    const loader = new URDFLoader(manager);

    // Added by Orwa to support loading gltf files to simple.html
    loader.loadMeshCb = function(path, manager, onComplete) {

        const gltfLoader = new GLTFLoader(manager);
        gltfLoader.load(
            path,
            result => {
                const model = result.scene;
                onComplete(model);
            },
            undefined,
            err => {
                // try to load again, notify user, etc
                onComplete(null, err);
            },
        );
    };

    loader.load(`${
        location.host !== 'rwth-crmasters-sose22.gitlab.io' ? '../../' : './'
    }urdf/bagger/urdf/bagger_test.urdf`, result => {
        robot = result;
    });

    // wait until all the geometry has loaded to add the model to the scene
    manager.onLoad = () => {
        // robot.up = {
        //    x: 0,
        //    y: 0,
        //    z: 1,
        // };
        // robot.rotation.x = -Math.PI / 2;

        for (const [, value] of Object.entries(robotJointInfo)) {
            robot.joints[value[0]].setJointValue(value[1]);
        }
        // robot.updateMatrixWorld(true);

        const bb = new Box3();
        bb.setFromObject(robot);

        scene = new Scene();
        scene.background = new Color(0x263238);

        camera = new PerspectiveCamera();
        camera.position.set(
            (bb.min.x + bb.max.x) * 2,
            (bb.min.y + bb.max.y) * 2,
            (bb.min.z + bb.max.z) * 2,
        );
        camera.up = new Vector3(0, 0, 1);
        camera.lookAt(
            (bb.min.x + bb.max.x) / 2,
            (bb.min.y + bb.max.y) / 2,
            (bb.min.z + bb.max.z) / 2,
        );

        renderer = new WebGLRenderer({ antialias: true });
        renderer.outputEncoding = sRGBEncoding;
        renderer.shadowMap.enabled = true;
        renderer.shadowMap.type = PCFSoftShadowMap;
        document.body.appendChild(renderer.domElement);

        const directionalLight = new DirectionalLight(0xffffff, 0.6);
        directionalLight.castShadow = true;
        directionalLight.shadow.mapSize.setScalar(1024);
        directionalLight.position.set(5, -8.5, 30);
        scene.add(directionalLight);

        /*
        const directionalLightSecondary = new DirectionalLight(0xffffff, 0.25);
        directionalLightSecondary.castShadow = false;
        directionalLightSecondary.shadow.mapSize.setScalar(1024);
        directionalLightSecondary.position.set(-20, 10, 20);
        scene.add(directionalLightSecondary);
        */

        const ambientLight = new AmbientLight(0xffffff, 0.2);
        scene.add(ambientLight);

        const ground = new Mesh(new PlaneBufferGeometry(), new ShadowMaterial({ opacity: 0.25 }));
        // ground.rotation.x = -Math.PI / 2;
        ground.scale.setScalar(30);
        ground.receiveShadow = true;
        scene.add(ground);

        ball = new Mesh(
            new SphereGeometry(0.1, 18, 12),
            new MeshBasicMaterial({ color: 0xffff00 }),
        );
        ball.position.x = (bb.min.x + bb.max.x) / 2;
        ball.position.y = bb.min.y * 1.15;
        ball.position.z = (bb.min.z + bb.max.z) / 2;

        ball2 = new Mesh(
            new SphereGeometry(0.1, 18, 12),
            new MeshBasicMaterial({ color: 0xff00ff }),
        );
        ball2.position.x = (bb.min.x + bb.max.x) / 2;
        ball2.position.y = bb.min.y * 1.15;
        ball2.position.z = (bb.min.z + bb.max.z) / 2;

        console.log(`%ctarget position set to ${ JSON.stringify(ball.position) }`, si);

        controls = new OrbitControls(camera, renderer.domElement);
        controls.minDistance = 4;
        controls.target.x = (bb.min.x + bb.max.x) / 2;
        controls.target.y = (bb.min.y + bb.max.y) / 2;
        controls.target.z = (bb.min.z + bb.max.z) / 2;
        controls.update();

        // robot.position.y -= bb.min.y;
        scene.add(robot);

        onResize();
        window.addEventListener('resize', onResize);
        window.addEventListener('keypress', (kbEvent) => { if (kbEvent.key === 'D') toggleDebug(); });
        window.addEventListener('keypress', (kbEvent) => { if (kbEvent.key === 'T') toggleTarget(); });
        render();
        debugThread();
    };

    document.getElementById('sessionMQTTBroker').classList.add('disconnected');
    console.log(`Connecting to broker ${brokerConnectParams[0]}...`);
    client.connect(brokerConnectOptions);
}

function onResize() {

    renderer.setSize(window.innerWidth, window.innerHeight);
    renderer.setPixelRatio(window.devicePixelRatio);

    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();

}

function render() {

    requestAnimationFrame(render);
    renderer.render(scene, camera);

}

function debugThread() {
    if (robot.joints) {
        let debugString = '';
        for (const [jointName, joint] of Object.entries(robot.joints)) {
            if (joint.jointType === 'fixed') continue;
            debugString += `${ jointName } (${ joint.jointType }): \n\t${ joint.angle }\n`;
        }
        document.getElementById('debug').innerText = debugString;
        // console.dir(robot.joints);
    }

    setTimeout(debugThread, 500);
}
