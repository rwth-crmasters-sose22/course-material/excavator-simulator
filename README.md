# Web-Based Excavator Simulator 

> Created for the CR Master's DSSIPD Class by _Orwa Diraneyya_

This repo is a forked from the [urdf-loaders](https://github.com/gkjohnson/urdf-loaders) repo on GitHub.

## Introduction

To communicate with the excavator instance in the browser, you need to copy the token shown in the upper-right corner of the simulator view, which will be used to create a unique MQTT topic for controlling the model.

![](./docs/excavator-simulator-token.png)

## MQTT Broker

In order to be able to send commands successfully to your excavator instance,please use the same broker URL indicated in the interface, along with the following port number, depending on your needs (information obtained from [this link](https://www.emqx.com/en/mqtt/public-mqtt5-broker)):

- TCP port: 1883
- Websocket Port: 8083
- TCP Port (secure): 8883
- Websocket Port (secure): 8084

Note that NodeRED MQTT nodes usually use a plain (i.e. non-secure) TCP connection. So please, use that port in the configuration of your NodeRED _MQTT in_ and _MQTT out_ nodes.

## MQTT Topic

The topic used to control the excavator instance can be seen by using the "inspect" feature in the browser, and switching to the _console_ tab:

![](./docs/excavator-simulator-console-topic.png)

Alternatively to hard-coding the topic in NodeRED, one can use the "template" node using the following _mustache template_ (please search the node palette in NodeRED using the word **template**):

```
rwth/bcma/sessions/{{{token}}}/command
```

The tricky part above is the use of three curly braces instead of two. In the NodeRED template node, this prevents transforming some of the characters in the token and hence stands for a "literal insertion" of the token in that position of the string.

For more information about the template node, please watch the short video made by your teacher on Moodle. 
## MQTT Message Payload

The MQTT message sent to the topic above should be a JSON string. JSON strings are converted from a JavaScript object.

To store a JSON object inside of payload, you can either use the **Inject** or the **Change** node, by setting the type of the field to JSON.

For more information please watch the short video made by your teacher on Moodle.

### MQTT Message Payload Fields

The payload sent over MQTT must correspond to an object with the following attributes:

| field   | type | value |
|---------|------|-------|
| `command` | string | `setJointValue` |
| `jointName` | string | name of joint from joint table |
| `jointValue` | number | value of joint within ranges in the joint table |
## Joint Information

This is a list of the joint names and ranges to control the excavator nicely:

| joint name | min value | max value |
|------------|:---------:|:---------:|
| q0 | -1.57 | +1.57 |
| q1 | -0.785 | +0.785 |
| q2 | -0.2 | 0.0 |
| q3 | -0.41 | 0.0 |
| q4 | -0.32 | 0.0 |
| q5 | 0.0 | 0.14 |
# LICENSE

The software is available under the [Apache V2.0 license](./LICENSE).

Copyright © 2020 California Institute of Technology. ALL RIGHTS
RESERVED. United States Government Sponsorship Acknowledged.
Neither the name of Caltech nor its operating division, the
Jet Propulsion Laboratory, nor the names of its contributors may be
used to endorse or promote products derived from this software
without specific prior written permission.


